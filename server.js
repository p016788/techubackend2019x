var express = require('express'),
  app = express(),
  port = process.env.PORT || 3003;


var bodyParser = require('body-parser');
var cors = require('cors');
app.use(bodyParser.json());
app.use(cors());

//se comenta temporalmente para superar prueba de error cors
/*
app.use(function (req, res, next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Request-With,Content-Type,Accept");
  next();
})*/

var requestjson = require('request-json');
var path = require('path');
var urlClientes    = "https://api.mlab.com/api/1/databases/lcastillop/collections/clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlClientesPut = "https://api.mlab.com/api/1/databases/lcastillop/collections/clientes/";
var urlMovimientos = "https://api.mlab.com/api/1/databases/lcastillop/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlProductos   = "https://api.mlab.com/api/1/databases/lcastillop/collections/productos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlCotizaUsd   = 'https://www.deperu.com/api/rest/cotizaciondolar.json';
var apiBdLlave       = 'apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';

//Se define como variables globales
var movimientosMlab   = requestjson.createClient(urlMovimientos);
var productosMlab     = requestjson.createClient(urlProductos);
var clientesMlab      = requestjson.createClient(urlClientes);
var cotizaUsdMlab     = requestjson.createClient(urlCotizaUsd);

//Con esto validamos en consola que el backend esta iniciado
app.listen(port);
console.log('todo list RESTful API server started on: ' + port);

//con esto validamos que el backend este iniciado en el browser para realizar las peticiones
app.get("/", function(req,res){
  res.sendFile(path.join(__dirname, 'index.html'));
})

//-------------- APIs de Movimientos
//-------------------- get consulta de movimientos
app.get('/movimientos',function(req, res){
  console.log(`get /movimientos/`)
  movimientosMlab.get('', function(err, resM, body){
    if(err) {
      console.log(err);
    } else {
      res.send(body);
    }
  })
})

//------------------ post alta de movimientos
app.post('/movimientos', function(req, res){
  console.log(`post /movimientos/`)
  movimientosMlab.post('', req.body, function(err, resM, body){
    res.send(body);
  })
})

//------------------GET MOVIMIENTOS DE UN CONTRATO (REQUIERE NUMERO DE CONTRATO)
app.get('/v1/movimientos/:accountNumber', function(req, res){
  console.log(`get /v1/movimientos/`+req.params.accountNumber)
  //comentamos temporalmente para validar el api
  var queryParam = "q={'alias':'"+req.headers.alias+"','accountNumber':'"+req.params.accountNumber+"'}";
  //var queryParam = "q={'numcontrato':'"+req.params.numcontrato+"'}";
  var getting=(urlMovimientos+"&"+queryParam);
  movimientosMlab.get(getting, function(err, resM, body){
    if(!err){
        if(body.length>0){
          res.status(200);
          res.send(body);
        }else{
          res.status(204);
          res.send(null);
        }
    }else{
      res.status(500)
      res.send(err);
    }
  })
})

//-------------- APIs de Productos
//-------------------get consulta de productos
app.get('/productos',function(req, res){
  console.log(`get /productos`)
  productosMlab.get('', function(err, resM, body){
    if(err) {
      console.log(err);
    } else {
      res.send(body);
    }
  })
})

//------------------- post alta de productos
app.post('/productos', function(req, res){
  console.log(`post /productos`)
  productosMlab.post('', req.body, function(err, resM, body){
    res.send(body);
  })
})

//-----------------GET de productos en base a un numero de contrato
app.get('/v1/productos',function(req, res){
  console.log('get /v1/productos'+req.headers.alias);
  var queryParam = "q={'alias':'"+req.headers.alias+"'}";
  var getting=(urlProductos+"&"+queryParam);
  productosMlab.get(getting, function(err, resM, body){
    if(!err){
        if(body.length>0){
          res.status(200);
          res.send(body);
        }else{
          res.status(204);
          res.send(null);
        }
    }else{
      res.status(500)
      res.send(err);
    }
  })
})

//-------------- APIs de Clientes
//---------------------get consulta de clientes
app.get('/clientes',function(req, res){
  console.log('get /clientes');
  clientesMlab.get('', function(err, resM, body){
    if(err) {
      console.log(err);
    } else {
      res.send(body);
    }
  })
})

//--------------------- post alta de clientes
app.post('/clientes', function(req, res){
  console.log('post /clientes');
  clientesMlab.post('', req.body, function(err, resM, body){
    res.send(body);
  })
})

//----------------------- GET consulta por tipo y numero de documento
app.get('/v1/clientes/:alias', function(req, res){
  console.log(`get /v1/clientes/`+req.params.alias);
  var alias   = req.params.alias;
  var queryParam = "q={'alias':'"+alias+"'}";
  var filter = "f={'_id':0}"; //Se filtra el _id para no mostrarlo en la consulta
  var getting=(urlClientes+"&"+queryParam+"&"+filter);

  clientesMlab.get(getting, function(err, resM, body){
    if(!err){
        if(body.length>0){
          res.status(200);
          res.send(body);
        }else{
          res.status(204);
          res.send(null);
        }
    }else{
      res.status(500)
      res.send(err);
    }
  })
})

//------------------------POST Alta de clientes
app.post('/v1/clientes', function(req, res){
  console.log(`post /v1/clientes`);
  var data = req.body;

  clientesMlab.post(urlClientes, data, function(err, resM, body) {
    if(!err){
        if(body != null){
          res.status(200);
          res.send(body);
        }else{
          res.status(204);
          res.send(null);
        }
    }else{
      res.status(500)
      res.send(err);
    }
  })
})
//----------------- put modificacion de clientes (se modifica en base al _id.$oid)
app.put('/v1/clientes/:alias', function(req, res){

  console.log(`put /v1/clientes/`+req.params.alias);

  var data = { "$set" : req.body };
  var queryParam = "q={'alias':'"+req.params.alias+"'}";
  var getting=(urlClientes+"&"+queryParam);

  clientesMlab.get(getting, function(errGet, resGet, bodyGet) {
    if(!errGet){
      if(bodyGet !=null && bodyGet.length > 0){
        clientesMlab.put(urlClientesPut+bodyGet[0]._id.$oid+"?"+apiBdLlave, data, function(errPut, resPut, bodyPut) {
          if(!errPut){
              if(resPut.statusCode == 200){
                res.status(200);
                res.send(bodyPut);
              }else{
                res.status(204);
                res.send(null);
              }
          }else{
            res.status(500)
            res.send(errPut);
          }
        });
      }else{
        res.status(204);
        res.send(null);
      }
    }else{
      res.status(500)
      res.send(err);
    }
  })
})
//----------------- Delete de clientes (se elimina en base al _id.$oid)
app.delete('/v1/clientes/:alias', function(req, res){
  console.log(`delete /v1/clientes/`+req.params.alias);
  var data = req.body;
  var queryParam = "q={'alias':'"+req.params.alias+"'}";
  var getting=(urlClientes+"&"+queryParam);

  clientesMlab.get(getting, function(err, resM, body) {
    if(!err){
      if(body !=null && body.length > 0){
        clientesMlab.del(urlClientesPut+body[0]._id.$oid+"?"+apiBdLlave, function(err, resM, body) {
          if(!err){
            if(resM.statusCode == 200){
              res.status(200);
              res.send(null);
            }else{
              res.status(204);
              res.send(null);
            }
          }else{
            res.status(500)
            res.send(err);
          }
        })
      }else{
        res.status(204);
        res.send(null);
      }
    }else{
      res.status(500)
      res.send(err);
    }
  })
})

//---------------- APIs de Cotiza USD
app.get('/cotizausd', function(req, res){

  console.log(`get /cotizausd`);

  cotizaUsdMlab.get(urlCotizaUsd, function(err, resM, body){
    if(!err){
      if(body != null){
          var data = {'compra':body.Cotizacion[0].Compra,'venta':body.Cotizacion[0].Venta};
          res.status(200);
          res.send(data);
        }else{
          res.status(204);
          res.send(null);
        }
      }else{
        res.status(500)
        res.send(err);
      }
  })
})

//--------------- APIs de login
//-----------------POST (requiere tipdoc,numdoc y password en el body)
app.post('/v1/login', function(req, res){
  console.log('/v1/login/'+req.body.user+'/'+req.body.password);
  var alias    = req.body.user;
  var password = req.body.password;
  var queryParam = "q={'alias':'"+alias+"','password':'"+password+"'}";
  var filter = "f={'_id':0,'password':0}";
  var getting=(urlClientes+"&"+queryParam+"&"+filter);

  clientesMlab.get(getting, function(err, resM, body){
    if(!err){
        if(body.length>0){
          res.status(200);
          res.send(body);
        }else{
          res.status(204);
          res.send(null);
        }
    }else{
      res.status(500)
      res.send(err);
    }
  })
})
//-------------------------- Error generico
app.use(function(req, res, next){
  res.status(404);
  res.format({
    json: function () {
      res.json({ error: 'Resource not found' })
    }
  })
});
